#FORMAS DE CREAR DICCIONARIOS:
a = dict(one = 1, two = 2, three = 3)
print(a)

print(a["one"])

#se puede usar el zip

c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
print(c)

#si se crea un diccionario vacio puedes ir añadiendo cosas:

dic = {}
dic["one"] = 7
print(dic)