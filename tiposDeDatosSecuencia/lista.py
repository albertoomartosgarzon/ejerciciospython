

#--------------------CREACION DE LISTAS---------------------


#>>> lista3 = list()
#  >>> lista4 = list("hola")
# >>> lista4 
# ESTO ES LO Q SALDRIA POR PANTALLA ['h', 'o', 'l', 'a']

#--------------------METODOS PARA LAS LISTAS---------------------

#sorted(NOMBRE DE TU LISTA) te lo ordena 
#sorted(NOMBRE DE TU LISTA, reverse=True) te lo ordena al reves
#len(NOMBRE DE TU LISTA) te dice el tamaño de tu lista
#sorted(NOMBRE DE TU LISTA) te lo ordena
#sorted(NOMBRE DE TU LISTA) te lo ordena


#--------------------SLIDE---------------------


#lista [:] TE MUESTRA TODOS LOS ELEMENTOS DE TU LISTA
#lista[posicion en la q comienza:posicion en la q termina:intervalo(cantidad de cosas q devuelve)] EJEMPLO

#IMPORTANTE LOS RANGOS FUNCIONAN DE LA MISMA MANERA Q ARRIBA PERO SEPARADOS POR COMAS!!!!!!!!!!!!!!!!!!

#lista [1,2,3] 
#lista[0:1] ME DEVUELVE UN UNO Y UN DOS Y SI LE PUSIERA LOS DOS PUNTOS Y UN DOS ME DEVOLVERIA EL SLIDE DE DOS EN DOS


#--------------------COPIADO DE LISTAS---------------------

#lista2 = lista1[:] ESTO ME COPIA LA LISTA SIN HACER REFERENCIA A LA PRIMERA POR LO Q SI CAMBIO LA PRIMER NO ME CAMBIARA LA SEGUNDA
#lista2 = lista1 PERO SI LO PONGO ASI SI CAMBIO LA PRIMERA SE CAMBIA LA SEGUNDA
#lista2 = lista1.copy()

#in ,not in


#--------------------OPCIONES AVANZADAS---------------------

#filter===> lista = [1,2,3,4,5]
# def par(x): return x % 2==0 
# list(filter(par,lista)) COMO PODEMOS VER HEMOS FILTRADO UNA LISTA MEDIANTE UNA FUNCION Q NOS DEVULVE SI ES PAR O NO


#listas comprimidas [x for x in range(10) if x % 2 == 0]
# NOS DEVUELVE ESTO[0, 2, 4, 6, 8] 