
#uso del try catch pero aqui es except y la excepcion al lao 
x= 0
#he usado el not para indicar que la x esta vacia
while not x:
    try:
        x = int(input("Dame un numero: "))
        break
    #Hay varios tipos de excepciones y si hay varias se ponen asi:
    #o se pone otro except XXX: con una excepcion 
    #o se agrupan except (RUntimeException,ValueError,......)
    except ValueError:
        #si en vez de poner un pint pongo un rise me salta excepcion como si fuese propia del sistema
       ''' raise print("Debes pasar un numero!!")
        print("Debes pasar un numero!!")'''
        #esta linea de abajo solo se puede poner sin try catch dentro de el no salta bien PARA CUANDO NO TENGA TRY CATCH LO DE ABAJO 
        raise ValueError("NO es un numero, debes pasarme un numero")
        #Exception es la padre de todas, si la pones recoges cualquier excepcion
        raise Exception("NO es un numero, debes pasarme un numero")

    #el finally es como en java se ejecuta igualmente aun q salte la excepcion

print(x)
