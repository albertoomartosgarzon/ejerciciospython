class EsUnMono:
    esUnMono = True

class Animales:
    patas = True
    numeroDePatas = 0
    pelo = True
    mamifero = True

    def esMamifero(self):
        if mamifero == True:
            print("Es un mamifero")
        else:
            print("NO es un mamifero")

#Herencia simple

class Pajaros(Animales):
    mamifero = False

#Herencia multiple

class Mono(Animales,EsUnMono):
    mamifero = True


gorrion = Pajaros()
print("¿Es un mamifero?",gorrion.mamifero)

mono = Mono()
print("¿Es un mamifero?",mono.mamifero,"¿Y es un mono?",mono.esUnMono)


