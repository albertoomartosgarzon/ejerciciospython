#Preguntar esto a Miguel

from punto.py import Punto

class circulo():    

    def __init__(self,centro,radio):
        self.centro=centro
        self.radio=radio    

    def __str__(self):
        return "Centro:{0}-Radio:{1}".format(self.centro.__str__(),self.radio)    

#Como podemos ver la clase punto ayuda para obtener el punto que queriamos para el centro del circulo y se mete dentro del constructor osea 
# que la clase circulo delega obtener el objeto punto del centro a la clase punto 
circulo = circulo(Punto(2,3),5)
print(circulo)