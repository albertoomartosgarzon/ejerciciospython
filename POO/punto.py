import math
class Punto():

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
    def distancia(self, otro):
        dx = self.x - otro.x
        dy = self.y - otro.y
        return math.sqrt((dx*dx + dy*dy))

punto1 = Punto()
punto2 = Punto(4,5)
print(punto1.distancia(punto2))


class punto3d(punto):
    def __init__(self,x=0,y=0,z=0):
        #Preguntar a Miguel si es necesario hacer el super (creo q es como en Java)
        super().__init__(x,y)
        self.z=z
    @property
    def z(self):
        return self._z    

    @z.setter
    def z(self,z):
        self._z=z    

    def __str__(self):
        return super().__str__()+":"+str(self.z)    

    def distancia(self,otro):
        dx = self.x - otro.x
        dy = self.y - otro.y
        dz = self.z - otro.z
        return (dx*dx + dy*dy + dz*dz)**0.5   