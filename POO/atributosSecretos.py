class Alumno():
    contador = 0
    def __init__(self,nombre=""):
        self.nombre = nombre
        #esto se supone que es un atributo privado 
        self.__secreto = 'asdfa'
        #self._secreto = 'asdfa' ======================> Este tambn pero es mas facil de acceder
        Alumno.contador

al = Alumno("Pepe")
#al de abajo se accederia asi ====> print(al_secreto)
print(al._Alumno__secreto)


#MUY IMPORTANTES LO GETTER Y LOS SETTERS DE LOS ATRIBUTOS Y AYUDAN LOS DELETE Y LOS HASATTR

print(getattr(al,"nombre"))
setattr(al,"nombre","Alberto")
print(getattr(al,"nombre"))
print(hasattr(al,"nombre"))
delattr(al,"nombre")
print(getattr(al,"nombre"))

#----------------------------------------------------------------- Preguntar a Miguel las diferencias entre lo de arriba y lo de abajo 

class circulo():
    def __init__(self,radio):
        self.set_radio(radio)
    def set_radio(self,radio):
        if radio>=0:
            self._radio = radio
        else:
            raise ValueError("Radio positivo")
            self._radio=0
    def get_radio(self):
        print("Estoy dando el radio")
        return self._radio

    def radio(self):
        del self._radio

    def __eq__(self,otro):
        return self.radio==otro.radio

    def __str__(self):
    clase = type(self).__name__
    msg = "{0} de radio {1}"
    return msg.format(clase, self.radio)
