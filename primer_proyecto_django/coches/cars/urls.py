from django.urls import path, re_path

from .views import first_view, inicio, detail_view, concesionarioModificado, concesionarioCreado

urlpatterns = [
    path('', inicio, name='inicio'),
    path('cars/', first_view, name='first_view'),
    path('detail/', detail_view, name='detail_view'),
    re_path(r'^concesionario/editado/(?P<pk>[0-9])/$', concesionarioModificado, name = 'concesionarioModificado'),
    path('concesionario/creado/', concesionarioCreado, name='concesionarioCreado'),

]
