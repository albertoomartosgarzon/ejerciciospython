from django.contrib import admin

# Register your models here.

from .models import Cars, TipoDeTraccion, Concesionario
#esto sirve para el admin de la url que nos aparezca en la bbdd de la aplicacion web en el buscador
admin.site.register(Cars)
admin.site.register(TipoDeTraccion)
admin.site.register(Concesionario)
