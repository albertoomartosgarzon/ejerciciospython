from django.db import models


# Create your models here.


class Concesionario(models.Model):
    nombre = models.CharField('Concesionario', max_length=50)
    idConcesionario = models.IntegerField('ID del concesionario', unique=True)


class Cars(models.Model):
    ColorNegro = 1
    ColorAzul = 2
    ColorBlanco = 3
    COLOR_ESCOGIDO = (
        (ColorNegro, "NEGRO"),
        (ColorAzul, "AZUL MARINO"),
        (ColorBlanco, "BLANCO")
    )
    # LO QUE HAY ENTRE COMILLAS ES LO QUE TE INDICA QUE TIENES QUE RELLENAR EN EL FORMULARIO DEL ADMIN
    modelo = models.CharField('Modelo', max_length=50)
    marca = models.CharField('Marca', max_length=50)
    km = models.DecimalField('Kilometraje del coche', max_digits=8, default=0, decimal_places=2)
    esNuevo = models.BooleanField('¿Es nuevo?', default=True)
    color = models.PositiveSmallIntegerField('Color', choices=COLOR_ESCOGIDO, default=ColorNegro)
    # imagen = models.ImageField("Imagen", blank = True, null = True, upload_to = imagen_upload_localizacion)
    # preguntar a miguel como funciona el related name de abajo pero en principio
    # el related name sirve para saber cuantos coches tiene mi concesionario no solo de que concesionario viene mi coche (para que funcione al reves la fk)
    concesionario = models.ForeignKey(Concesionario, related_name="concesionario", on_delete=models.CASCADE)

    # no se para que sirve lo del meta tengo que preguntarle a miguel

    class Meta:
        verbose_name = "Car"
        verbose_name_plural = "Cars"
        ordering = ['-modelo']

    def __str__(self):
        return self.modelo


class TipoDeTraccion(models.Model):
    traccion = models.CharField('Tipo de traccion', max_length=20)
    cars = models.ManyToManyField(Cars, blank=True, related_name="traccion")
