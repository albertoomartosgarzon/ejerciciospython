from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from .forms import ConcesionarioForm
from .models import Cars, Concesionario


# Create your views here.
#esto de aqui abajo es para que tengas que estar registrado si quieres acceder a esa pagina
@login_required
def first_view(request):
    cocheLista = Cars.objects.all()
    cochesContados = cocheLista.count()
    print('cuantos coches tengo:', cochesContados)
    print('existe?', cocheLista.filter(id=1))
    print('existe booleano?', cocheLista.filter(id=1).exists())

    # -----------------------CREATE--------------------------

   # concesionarioNuevo = Concesionario.objects.create(nombre="opel", idConcesionario=3)
   # Cars.objects.create(modelo='nuevoModelosCreadoDesdeLaVista', concesionario = concesionarioNuevo)
    # no he conseguido listarlo con el modelo
   # print('esta bien creado?', cocheLista.filter(id=3))

    # -----------------------GET--------------------------

    print('bien filtrado?', cocheLista.filter(concesionario__nombre__startswith = "Ca"))
    print('AND?', cocheLista.filter(concesionario__nombre__startswith = "Ca", km = 0))
    print('OR?', cocheLista.filter(Q (concesionario__nombre__startswith = "Ca") | Q ( km = 0)))

    #-----------------------DELETE--------------------------


    #Cars.objects.get(id=3).delete()
    #print('esta bien borrado bien?', cocheLista.filter(id=3))

    # -----------------------UPDATE--------------------------
   # car = Cars.objects.get(modelo = "SINMODIFICAR")
  #  print("coche para actualizar", car)
    #car.modelo = "MODELOMODIFICADO"
  #  car.save()  ------------------ se puede poner un .add() y añadirle un objeto concesionario o algo mas
   # print(car)
    idRecorrido = []
    for cars in cocheLista:
        idRecorrido.append(cars.id)
        print('Esta es mi lista!!!!!!!!!!!!!!!!!!!!', idRecorrido)




    context = {
        'coches_list': cocheLista,
    }

    return render(request, 'cars.html', context)




def detail_view(request):
    context = {
        'coches_detail': 'hola'
    }

    return render(request, 'detail.html', context)

@login_required
def inicio(request):
    return render(request, 'inicio.html')


def concesionarioModificado(request, pk):
    concesionario = get_object_or_404(Concesionario, pk = pk)
    if request.method == 'POST':
        form = ConcesionarioForm(request.POST)
        if form.is_valid():
            concesionario.nombre = form.cleaned_data['nombre']
            concesionario.idConcesionario = form.cleaned_data['idConcesionario']
            concesionario.save()
    else:
        #el initial es para que te de los valores de la base de datos
        form = ConcesionarioForm(
            initial={
            'nombre': concesionario.nombre,
            'idConcesionario': concesionario.idConcesionario
        }
        )

    context = {
        'concesionario': concesionario,
        'form': form
    }

    return render(request, 'ConcesionarioForm.html', context)


def concesionarioCreado(request):

    concesionarioNuevo = Concesionario()

    if request.method == 'POST':
        form = ConcesionarioForm(request.POST)
        if form.is_valid():
            nombreNuevo = form.cleaned_data['nombre']
            idConcesionarioNuevo = form.cleaned_data['idConcesionario']
            concesionarioNuevo = Concesionario.objects.create(nombre=nombreNuevo, idConcesionario=idConcesionarioNuevo)
            concesionarioNuevo.save()
    else:
        #el initial es para que te de los valores de la base de datos
        form = ConcesionarioForm()

    context = {
        'concesionario': concesionarioNuevo,
        'form': form
    }

    return render(request, 'ConcesionarioCreadoForm.html', context)