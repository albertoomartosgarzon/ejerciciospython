from django import forms


class ConcesionarioForm(forms.Form):
    nombre = forms.CharField(required = True)
    idConcesionario = forms.IntegerField(required = True, label = "ID", initial = 0)

