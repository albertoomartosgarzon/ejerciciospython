#Lectura de ficheros .txt
#r+ es lectura y escritura
#w es solo escritura
#r es solo lectura
f = open("lecturaYescrituraDeFIcheros.py/File/hola.txt","r+")


#asi se recorre un fichero

with open("lecturaYescrituraDeFIcheros.py/File/hola.txt","r+") as fichero:
    for linea in fichero:
        print(linea)

f.close()